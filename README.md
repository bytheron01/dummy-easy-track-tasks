# TODO APP

## What this app will do
The primary focus of the DETT app, is to make tasks / todo's easier to create/finish.

Features the app will focus on will be:
- CRUD of tasks/todo's
- User management
- Mobile usability
- Recieve notifications via email
- Recieve Google Chrome notifications
- Easy-to-use word editor in task creation: add images, emoticons, etc.