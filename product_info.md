# Breakdown of app

## Questions
1. What is the purpose of this app?
2. Who is it for?
3. Is there something out there already?
4. Why not use something that's already out there?
5. What makes my product/solution better?
6. Can I integrate with it?
7. Is it easy to use?

## Questions Anaswered
1. To be able to create tasks or TODO's in a very simple manner.
2. This will be targeted at people that are on the web for the majority of their business day.
3. Yes, there are quite a few services available.
4. Many of the services out there are a little over-kill for what I need.
5. This app is focused soley on tasks. No flashy extras. Straight to the point.
6. Yes, at the very lowest level of integrating -  sign into Gmail or Google Chrome (Web Browser)
7. Straight forward design, using the modern web standards for UI. No tutorial or onboarding courses required.